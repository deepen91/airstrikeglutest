﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AirStrikeGlu
{

    public class SceneLoadingManager : SingletonManager<SceneLoadingManager>
    {

        private LoadSceneMode mNextSceneLoadMode = LoadSceneMode.Single;

        public LoadSceneMode pNextSceneLoadMode { get { return mNextSceneLoadMode; } }

        private string mNextSceneName;
        public string pNextSceneName { get { return mNextSceneName; } }

        private string mLastSceneName;
        public string pLastSceneName { get { return mLastSceneName; } }

        public string LoadingSceneName = "MainMenu";


        public void SwitchScene(string aSceneName, LoadSceneMode nextSceneLoadMode)
        {
            Debug.Log(":: SceneLoadingManager.cs :: SwitchScene :: SceneName : " + aSceneName);

            mLastSceneName = SceneManager.GetActiveScene().name;
            mNextSceneName = aSceneName;
            LoadingSceneName = aSceneName;

            mNextSceneLoadMode = nextSceneLoadMode;
            Debug.Log("<color=red> Switch to loading scene</color>");
            StartCoroutine(LoadScene(LoadingSceneName, mNextSceneLoadMode));
        }

        IEnumerator LoadScene(string aStrSceneName, LoadSceneMode aLoadSceneMode)
        {
            yield return null;

            //Begin to load the Scene you specify
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(aStrSceneName, aLoadSceneMode);
            //Don't let the Scene activate until you allow it to
            asyncOperation.allowSceneActivation = false;
            Debug.Log(":: Loading Scene : " + aStrSceneName + " :: Progress : " + asyncOperation.progress);
            //When the load is still in progress, output the Text and progress bar
            while (!asyncOperation.isDone)
            {
                //Output the current progress
                Debug.Log(":: Loading Scene : " + aStrSceneName + " :: Progress : " + (asyncOperation.progress * 100) + "%");

                // Check if the load has finished
                if (asyncOperation.progress >= 0.9f)
                {
                    Debug.Log(":: Loading Scene : " + aStrSceneName + " :: Progress : 100%");
                    //Activate next the Scene
                    asyncOperation.allowSceneActivation = true;
                }

                yield return null;
            }
        }

    }
}