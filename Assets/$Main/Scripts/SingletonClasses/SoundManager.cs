﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AirStrikeGlu
{
    public class SoundManager : SingletonManager<SoundManager>
    {
        [SerializeField]  private AudioClip[] _Clips;

        private AudioSource mAudioSource;

        private void Start()
        {
            if(mAudioSource == null)
            {
                mAudioSource = this.GetComponent<AudioSource>();
            }
        }
        /// <summary>
        /// Play the Sound with specific clip
        /// </summary>
        /// <param name="clip"></param>
        public void PlaySound(Defines.AudioClips clip)
        {
            if (PlayerPrefs.GetInt(GameKeys.GAME_SOUND_VALUE,1) == 0)
            {
                StopSound();
                return;
            }
            int index = ((int)clip) - 1;
            mAudioSource.clip = _Clips[index];
            mAudioSource.Play();
        }
        /// <summary>
        /// Stop the sound if playing
        /// </summary>
        public void StopSound()
        {
            if (mAudioSource.isPlaying)
            {
                mAudioSource.Stop();
            }
        }

        /// <summary>
        /// Responsible To Update Volume
        /// </summary>
        /// <param name="aVal"></param>
        public void UpdateSoundVolume(float aVal)
        {
            PlayerPrefs.SetFloat(GameKeys.GAME_SOUND_VOLUME,aVal);
            mAudioSource.volume = aVal;
        }

    }
}