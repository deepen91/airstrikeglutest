﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirStrikeGlu
{
    public class ProfileManager : SingletonManager<ProfileManager>
    {
        #region User Highest Score
        public void SetPlayerScoreToProfile(int aScore)
        {
            int highestScore = PlayerPrefs.GetInt(GameKeys.GAME_HIGHEST_SCORE,0);
            if (aScore > highestScore)
            {
                PlayerPrefs.SetInt(GameKeys.GAME_HIGHEST_SCORE, aScore);
            }
        }
        public int GetPlayerHighestScore()
        {
            int highestScore = PlayerPrefs.GetInt(GameKeys.GAME_HIGHEST_SCORE, 0);
            return highestScore;
        }
        #endregion

        #region User Last Level
        public void SetCurrentGameLevel(int aLevel)
        {   
            PlayerPrefs.SetInt(GameKeys.GAME_CURRENT_LEVEL, aLevel);            
        }
        public int GetCurrentGameLevel()
        {
            return PlayerPrefs.GetInt(GameKeys.GAME_CURRENT_LEVEL, 0);
        }
        #endregion

        #region User Current Flight 
        public void SetCurrentGameFlight(int aFlightIndex)
        {
            PlayerPrefs.SetInt(GameKeys.GAME_CURRENT_FLIGHT, aFlightIndex);
        }
        public int GetCurrentGameFlightUsed()
        {
            return PlayerPrefs.GetInt(GameKeys.GAME_CURRENT_FLIGHT, 0);
        }
        #endregion
    }
}
