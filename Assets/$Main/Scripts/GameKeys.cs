﻿public static class GameKeys
{
    public const string SCENE_LOADING = "LoadingScene";
    public const string SCENE_MAIN_MENU = "MainMenu";
    public const string SCENE_GAMEPLAY = "Gameplay";
    public const string GAME_HIGHEST_SCORE = "GAME_HIGHEST_SCORE";
    public const string GAME_SOUND_VALUE = "GAME_SOUND_VALUE";
    public const string GAME_SOUND_VOLUME = "GAME_SOUND_VOLUME";
    public const string GAME_CURRENT_LEVEL = "GAME_CURRENT_LEVEL";
    public const string GAME_CURRENT_FLIGHT = "GAME_CURRENT_FLIGHT";


}