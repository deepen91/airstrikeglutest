﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirStrikeGlu
{

    [CreateAssetMenuAttribute(fileName = "GameLevelData", menuName = "Level_Data")]
    public class GameLevelData : ScriptableObject
    {
        [SerializeField]
        public  List<LevelData> _List_LevelData;

        public List<LevelData> pList_LevelData
        {
            get
            {
                return _List_LevelData;
            }
        }

        [System.Serializable]
        public struct LevelData
        {
            public int Level;
            public DataInfo LevelStatsData;

        }

        [System.Serializable]
        public struct DataInfo
        {
            public int SmallEnemyCount;
            public int MediumEnemyCount;
            public float HealthMultiplier;
            public float BulletTimeTravelMultiplier;
            public int BulletShootFrequency;
            public float TimeIntervalToShootInSec;
            public int EnemySpawnFrequency;

            public int SmallEnemyDestroyPoint;
            public int MediumEnemyDestroyPoint;
            public int BossEnemyDestroyPoint;
            public EnemyOccuranceInfo InfoEnemyOccurance;

        }

        [System.Serializable]
        public struct EnemyOccuranceInfo
        {
            public Defines.EnemyFormationType CurrFormation;
            public int SmallEnemyOccuringCounter;
            public int MediumEnemyOccuringCounter;

        }



        public DataInfo GetLevelDataInfo(int Level)
        {
            DataInfo info = _List_LevelData.Find(x => x.Level == Level).LevelStatsData;
            return info;
        }
    }
}
