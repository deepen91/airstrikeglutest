﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AirStrikeGlu
{
    public class StaticGameData : SingletonManager<StaticGameData>
    {
        [SerializeField]
        private BulletData _BulletData;
        public BulletData pBulletData { get => _BulletData; }

        [SerializeField]
        private GameLevelData _LevelData;
        public GameLevelData pLevelData { get => _LevelData; }

        [SerializeField]
        private FighterPlaneData _FighterPlaneData;
        public FighterPlaneData pFighterPlaneData { get => _FighterPlaneData; }

        GameplayStats mStats;


        public void SetGameData(int aLevel)
        {
            mStats = new GameplayStats
            {
                Level = aLevel,
                PlayerPlaneType = GameManager.Instance.pCurrentFighterFlightUsed
            };
        }
        public GameplayStats GetGameplayStats()
        {
            return mStats;
        }

    }
}
