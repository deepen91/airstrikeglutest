﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;

[CreateAssetMenuAttribute(fileName = "BulletData", menuName = "Bullet_Data")]
public class BulletData : ScriptableObject
{
    [SerializeField]
    public List<Data> _List_BulletData;

    public List<Data> pList_BulletData
    {
        get
        {
            return _List_BulletData;
        }
    }

    [System.Serializable]
    public struct Data
    {
        public Defines.BulletType BulletType;
        public DataInfo BulletDataInfo;
    }

    [System.Serializable]
    public struct DataInfo
    {
        public float BulletTimeToReachDestination;
        public float BulletDamageUnit;
    }

    public DataInfo GetBulletDataInfo(Defines.BulletType aBulletType)
    {
        DataInfo info = _List_BulletData.Find(x => x.BulletType == aBulletType).BulletDataInfo;
        return info;
    }
}
