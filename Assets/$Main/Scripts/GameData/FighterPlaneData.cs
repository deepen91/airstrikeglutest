﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirStrikeGlu
{
    [CreateAssetMenuAttribute(fileName = "FighterPlaneData", menuName = "FighterPlane Data")]
    public class FighterPlaneData : ScriptableObject
    {
        [SerializeField]
        public List<PlayerFlightData> _List_PlayerFighterPlaneData;
        public List<PlayerFlightData> pList_PlayerFighterPlaneData
        {
            get
            {
                return _List_PlayerFighterPlaneData;
            }
        }
        [SerializeField]
        private List<EnemyFlightData> _List_EnemyFighterPlaneData;
        public List<EnemyFlightData> pList_EnemyFighterPlaneData
        {
            get
            {
                return _List_EnemyFighterPlaneData;
            }
        }
        [System.Serializable]
        public struct EnemyFlightData
        {
            public Defines.EnemyFighterType FighterType;
            public FlightDataInfo FighterInfo;
        }

        [System.Serializable]
        public struct PlayerFlightData
        {
            public Defines.PlayerFighterType FighterType;
            public FlightDataInfo FighterInfo;
        }

        [System.Serializable]
        public struct FlightDataInfo
        {
            public int FighterID;
            public string FighterPlaneName;
            public float HealthPoints;
            public float FlyingSpeed;
            public List<Defines.BulletType> BulletShootType;
        }

        public FlightDataInfo GetEnemyFlightDataInfo(Defines.EnemyFighterType aEnemyFighterType)
        {
            FlightDataInfo info = pList_EnemyFighterPlaneData.Find(x => x.FighterType == aEnemyFighterType).FighterInfo;
            return info;
        }
        public FlightDataInfo GetPlayerFlightDataInfo(Defines.PlayerFighterType aPlayerFighterType)
        {
            FlightDataInfo info = pList_PlayerFighterPlaneData.Find(x => x.FighterType == aPlayerFighterType).FighterInfo;
            return info;
        }

    }


}