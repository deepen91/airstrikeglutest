﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AirStrikeGlu
{
    public class GameManager : SingletonManager<GameManager>
    {
        private Defines.PlayerFighterType currentFighterFlightUsed;
        public Defines.PlayerFighterType pCurrentFighterFlightUsed { get => currentFighterFlightUsed; set => currentFighterFlightUsed = value; }

        #region GamePause State
        bool isGamePause = false;
        public void OnPause(bool bValue)
        {
            isGamePause = bValue;
            if (bValue)
            {
                Time.timeScale = 0;
                LeanTween.pauseAll();
            }
            else
            {
                Time.timeScale = 1;
                LeanTween.resumeAll();
            }
        }
        public bool IsGamePause()
        {
            return isGamePause;
        }
        #endregion

        #region Gameplay Controller Property
        private GameplayController mGameplayController = null;
        public void SetGameplayController(GameplayController aGameplayController)
        {
            mGameplayController = aGameplayController;
            if (mGameplayController != null)
            {
                mGameplayController.UnRegisterGameStateChange(UpdatePlayerScore);
                mGameplayController.RegisterGameStateChange(UpdatePlayerScore);
            }
        }
        public GameplayController GetGameplayController()
        {
            return mGameplayController;
        }
        #endregion

        #region SetPlayerScoreToProfile
        private void UpdatePlayerScore(Defines.GameStates aState)
        {
            if (aState == Defines.GameStates.END)
            {
                int score = mGameplayController.GetPlayerTotalScore();
                ProfileManager.Instance.SetPlayerScoreToProfile(score);
                mGameplayController.UnRegisterGameStateChange(UpdatePlayerScore);
            }
        }
        #endregion


    }
}
