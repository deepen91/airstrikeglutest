﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AirStrikeGlu
{
    public class PlayerFighterPlane : FighterPlaneBase
    {
        [SerializeField]  private GameObject _FlightShield;

        [SerializeField]  private Defines.PlayerFighterType _PlayerFighterFlight;
        public Defines.PlayerFighterType pPlayerFighterFlight { get => _PlayerFighterFlight; }

        private ObjectPooler mObjPool = null;
        private Defines.BulletType mPlayerBulletType = Defines.BulletType.NONE;

        private float FlightHealth = 0;
        private bool isShieldActivated = false;

        private GameplayController mGameplayObj;
        private GameManager mGameManager;
        private GameLevelData.DataInfo mLevelInfo;


        #region Flighter PLane Health Event
        public UnityAction<float> OnHealthUpdatedEvent;
        public void RegisterPlayerHealthUpdated(UnityAction<float> aCallBack)
        {
            if (aCallBack != null)
            {
                OnHealthUpdatedEvent += aCallBack;
            }
        }
        public void UnRegisterPlayerHealthUpdated(UnityAction<float> aCallBack)
        {
            if (aCallBack != null)
            {
                OnHealthUpdatedEvent -= aCallBack;
            }
        }
        #endregion

        #region Flighter Plane Shield State Event
        public UnityAction<bool> OnShieldStateEvent;
        public void RegisterPlayerShieldState(UnityAction<bool> aCallBack)
        {
            if (aCallBack != null)
            {
                OnShieldStateEvent += aCallBack;
            }
        }
        public void UnRegisterPlayerShieldState(UnityAction<bool> aCallBack)
        {
            if (aCallBack != null)
            {
                OnShieldStateEvent -= aCallBack;
            }
        }
        #endregion

        public override void Awake()
        {
            base.Awake();
            mGameManager = GameManager.Instance;
            _FlightInfo = StaticGameData.Instance.pFighterPlaneData.GetPlayerFlightDataInfo(_PlayerFighterFlight);
            mPlayerBulletType = _FlightInfo.BulletShootType[0];
            FlightHealth = _FlightInfo.HealthPoints;
            int level = StaticGameData.Instance.GetGameplayStats().Level;
            mLevelInfo = StaticGameData.Instance.pLevelData.GetLevelDataInfo(level);
        }
        private void Start()
        {
            mGameplayObj = mGameManager.GetGameplayController();
            mObjPool = mGameplayObj.GetObjPooler();
            mObjPool.CreateBulletObjects(mPlayerBulletType);            
        }

        #region Base Class Functionality
        /// <summary>
        /// Bullet gets fire
        /// </summary>
        public override void FireBullet()
        {
            if (mPlayerBulletType != Defines.BulletType.NONE)
            {
                Vector3 ObjPos = this.gameObject.transform.position;

                mObjPool.SpawnFromPool(mPlayerBulletType, ObjPos, Quaternion.identity,true, ObjPos,1);//last parameter is the dirention in which the bullet will be sent. Nothing will be done in this player bullet case.
                mObjPool.SpawnFromPool(mPlayerBulletType, new Vector3(ObjPos.x - 20, ObjPos.y, ObjPos.z), Quaternion.identity, true, ObjPos,1);
                mObjPool.SpawnFromPool(mPlayerBulletType, new Vector3(ObjPos.x + 20, ObjPos.y, ObjPos.z), Quaternion.identity, true, ObjPos,1);
            }
        }
        /// <summary>
        /// invoke when the health is 0 or less
        /// </summary>
        public override void FighterFlightDestroyed()
        {
            //TODO show custom VFX when called.
            mGameplayObj.SetGameResult(Defines.GameResult.LOSE);
            mGameplayObj.SetCurrGameState(Defines.GameStates.END);
            base.FighterFlightDestroyed();
        }

        public override float GetHealthPoints()
        {
            return base.GetHealthPoints();
        }
        /// <summary>
        /// Method gets invoked everytime the bullet hits the Flight
        /// </summary>
        /// <param name="points"></param>
        public override void HitByBullet(float points)
        {
            base.HitByBullet(points);
            if (PlayerPrefs.GetInt(GameKeys.GAME_SOUND_VALUE, 1) == 1)
            {
                SoundManager.Instance.PlaySound(Defines.AudioClips.HIT_BY_BULLET);
            }
            FlightHealth -= points;
            CheckForHealth(FlightHealth);
            OnHealthUpdatedEvent(FlightHealth);
        }

        /// <summary>
        /// Create the player shield 
        /// </summary>
        public override void CreateShield()
        {
            base.CreateShield();
            SetShieldActiveState(true);
            _FlightShield.SetActive(true);
            StartCoroutine(HideShield(3f));
        }

        public override void AirStrike()
        {
            base.AirStrike();
        }
        /// <summary>
        /// Check the collision object
        /// </summary>
        /// <param name="collision"></param>
        public override void OnCollisionEnter(Collision collision)
        {
            if (mGameplayObj.GetCurrGameState() == Defines.GameStates.END)
                return;

            if (collision.gameObject.tag == "Enemy")
            {
                Debug.Log("player hit by enemy");
            }
            if (collision.gameObject.tag == "EnemyBullet")
            {
                collision.gameObject.SetActive(false);
                if (!isShieldActivated)
                {
                    Debug.Log("player hit by enemy bullet");
                    this.HitByBullet(collision.gameObject.GetComponent<Bullet>().pDamageUnits);
                }
            }
        }
        #endregion

        /// <summary>
        /// Check the fighter plane health
        /// </summary>
        /// <param name="flightHealth"></param>
        void CheckForHealth(float flightHealth)
        {
            if (flightHealth <= 0)
            {
                this.FighterFlightDestroyed();
            }
        }
        /// <summary>
        /// Hides the player shield after the specific duration of time
        /// </summary>
        /// <param name="aDelay"></param>
        /// <returns></returns>
        private IEnumerator HideShield(float aDelay)
        {
            yield return new WaitForSeconds(aDelay);
            SetShieldActiveState(false);
            _FlightShield.SetActive(false);
        }
        /// <summary>
        /// Gets if the shield is activated or not
        /// </summary>
        /// <returns></returns>
        public bool GetIsShieldActivated()
        {
            return isShieldActivated;
        }

        private void SetShieldActiveState(bool aState)
        {
            isShieldActivated = aState;
            if (OnShieldStateEvent != null)
            {
                OnShieldStateEvent.Invoke(aState);
            }
        }

    }
}
