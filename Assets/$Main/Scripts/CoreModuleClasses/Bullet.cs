﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;
public class Bullet : MonoBehaviour,IPooledObject
{

    [SerializeField]    private Defines.BulletType _BulletType;
    public Defines.BulletType pBulletType { get => _BulletType; }

    private float mDamageUnits;
    public float pDamageUnits { get => mDamageUnits; }

    private void Awake()
    {
        mDamageUnits = StaticGameData.Instance.pBulletData.GetBulletDataInfo(_BulletType).BulletDamageUnit;
    }

    public void OnPlayerBulletSpawn(Vector3 dir, float travelTime)
    {
        LeanTween.moveLocalZ(this.gameObject, 400, travelTime);
    }

    public void OnEnemyBulletSpawn(Vector3 dir, float travelTime)
    {
        Vector3 result = dir - this.transform.position;
        Vector3 newDis = result.normalized * 500;// Extend the bullet position in the same direction.

        LeanTween.moveLocal(this.gameObject, newDis, travelTime);
    }


}
