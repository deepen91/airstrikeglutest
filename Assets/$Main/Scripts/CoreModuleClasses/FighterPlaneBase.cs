﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AirStrikeGlu
{
    public abstract class FighterPlaneBase : MonoBehaviour
    {
        protected FighterPlaneData.FlightDataInfo _FlightInfo;

        public  virtual void Awake() {
        }

        public virtual void HitByBullet(float points) { }

        public virtual float GetHealthPoints()
        {
            return this._FlightInfo.HealthPoints;
        }

        public virtual void FighterFlightDestroyed()
        {
            LeanTween.cancel(this.gameObject);
            this.gameObject.SetActive(false);
        }
        public virtual void FireBullet() { }
        public virtual void CreateShield() { }
        public virtual void AirStrike() { }
        public virtual void OnCollisionEnter(Collision collision){ }

    }
}