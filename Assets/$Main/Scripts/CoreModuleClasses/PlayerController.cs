﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;
public class PlayerController : MonoBehaviour
{
    enum ERotateType
    {
        None,
        Left,
        Right
    }

    [SerializeField]    private List<PlayerFighterPlane> _ListPlayerPlanePool ;
    [SerializeField]    private float _PlayerFlightInitalPosZ;
    [SerializeField]    private float _PlayerFlightFinalPosZ;
    [SerializeField]    private float _PlayerFlightSpeedToEnterScreen;


    private float distance_to_screen;
    private float diff = 0;
    private float prevX = 0;
    private float NewX = 0;
    private float PosX;
    private float PosZ;

    private Camera _camera = null;

    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 pos_move;

    private Defines.BulletType mPlayerBulletType;

    private PlayerFighterPlane mPlayerFlight;
    public PlayerFighterPlane pPlayerFlight { get => mPlayerFlight; }

    private GameplayController mGameplayController;

    private GameObject mAssignedPlaneObj;
    private bool isMouseBtnPressed = false;

    private GameManager mGameManager;

    private void Awake()
    {
        mGameManager = GameManager.Instance;
        PlayerFighterPlane planeObj = _ListPlayerPlanePool.Find(x => x.pPlayerFighterFlight == mGameManager.pCurrentFighterFlightUsed);
        GameObject gObj = Instantiate(planeObj.gameObject);
        gObj.transform.SetParent(this.transform);
        gObj.transform.position = new Vector3(0,0, _PlayerFlightInitalPosZ);
        mAssignedPlaneObj = gObj;
        mPlayerFlight = mAssignedPlaneObj.GetComponent<PlayerFighterPlane>();


        mPlayerBulletType = Defines.BulletType.TYPE_A;
    }
    /// <summary>
    /// This method is responsible for movement of the plane from outside the screen to inside when the game started.
    /// </summary>
    void MoveAndEnterScreenSpace()
    {
        LeanTween.moveLocalZ(mAssignedPlaneObj, _PlayerFlightFinalPosZ, _PlayerFlightSpeedToEnterScreen).setOnComplete(()=> {
            mGameplayController.SetCurrGameState(Defines.GameStates.IN_PROGRESS);
        });
    }

    private void Start()
    {
        _camera = Camera.main;
        Vector3 pos = _camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, Screen.height));

        PosX = Mathf.Abs(pos.x) - mAssignedPlaneObj.transform.localScale.x;
        PosZ = Mathf.Abs(pos.z) - mAssignedPlaneObj.transform.localScale.z;

        mGameplayController = mGameManager.GetGameplayController();
        MoveAndEnterScreenSpace();
    }
    /// <summary>
    /// Handle the Movement and Rotation of the player
    /// </summary>
    private void Update()
    {
        if (mGameManager.IsGamePause())
            return;

        if (mAssignedPlaneObj == null)
            return;

        if (mGameplayController.GetCurrGameState() == Defines.GameStates.IN_PROGRESS)
        {
            mPlayerFlight.FireBullet();

            if (Input.GetMouseButtonDown(0))// 0 for left and 1 for right click
            {
                screenPoint = _camera.WorldToScreenPoint(mAssignedPlaneObj.transform.position);
                offset = mAssignedPlaneObj.transform.position - _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
                isMouseBtnPressed = true;
            }
            else if (Input.GetMouseButton(0) && isMouseBtnPressed)
            {
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    return;

                prevX = mAssignedPlaneObj.transform.position.x;

                Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
                Vector3 mouseWorldPosition = _camera.ScreenToWorldPoint(mousePosition) + offset;


                mAssignedPlaneObj.transform.position = mouseWorldPosition;

                CheckForObjectClamp();


            }
            else if (Input.GetMouseButtonUp(0))
            {
                isMouseBtnPressed = false;
                RotateObj(ERotateType.None);
            }
        }

    }
    void CheckForObjectClamp()
    {

        if (mAssignedPlaneObj.transform.position.x <= -PosX)
        {
            mAssignedPlaneObj.transform.position = new Vector3(-PosX, mAssignedPlaneObj.transform.position.y, mAssignedPlaneObj.transform.position.z);
        }
        if (mAssignedPlaneObj.transform.position.x >= PosX)
        {
            mAssignedPlaneObj.transform.position = new Vector3(PosX, mAssignedPlaneObj.transform.position.y, mAssignedPlaneObj.transform.position.z);
        }
        if (mAssignedPlaneObj.transform.position.z <= -PosZ)
        {
            mAssignedPlaneObj.transform.position = new Vector3(mAssignedPlaneObj.transform.position.x, mAssignedPlaneObj.transform.position.y, -PosZ);
        }
        if (mAssignedPlaneObj.transform.position.z >= PosZ)
        {
            mAssignedPlaneObj.transform.position = new Vector3(mAssignedPlaneObj.transform.position.x, mAssignedPlaneObj.transform.position.y, PosZ);
        }

    }

    /// <summary>
    /// Handle the Rotation of the player
    /// </summary>
    private void LateUpdate()
    {
        if (mGameManager.IsGamePause())
            return;
        if (mAssignedPlaneObj == null)
            return;

        if (mGameplayController.GetCurrGameState() == Defines.GameStates.IN_PROGRESS)
        {
            if (Input.GetMouseButton(0) && isMouseBtnPressed) // 0 for left and 1 for right click
            {
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    return;

                NewX = mAssignedPlaneObj.transform.position.x;
                diff = NewX - prevX;
                CheckForRotation();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                isMouseBtnPressed = false;
                RotateObj(ERotateType.None);
            }
        }
        
    }

    private void CheckForRotation()
    {
        if (diff < 0)
        {
            RotateObj(ERotateType.Left);
        }
        else if (diff > 0)
        {
            RotateObj(ERotateType.Right);
        }
        else
        {
            RotateObj(ERotateType.None);
        }
    }

    private void RotateObj(ERotateType aType)
    {
        LeanTween.cancel(mAssignedPlaneObj);
        switch (aType)
        {
            case ERotateType.None:
                LeanTween.rotateZ(mAssignedPlaneObj,0,0.5f);
                break;
            case ERotateType.Left:
                LeanTween.rotateZ(mAssignedPlaneObj, 30, 0.2f);
                break;
            case ERotateType.Right:
                LeanTween.rotateZ(mAssignedPlaneObj, -30, 0.2f);
                break;
            default:
                LeanTween.rotateZ(mAssignedPlaneObj, 0, 0.5f);
                break;
        }
    }


}
