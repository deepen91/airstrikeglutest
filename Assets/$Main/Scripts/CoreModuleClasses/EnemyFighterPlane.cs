﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace AirStrikeGlu
{
    public class EnemyFighterPlane : FighterPlaneBase
    {
        [SerializeField] private Defines.EnemyFighterType _EnemyFighterFlight;
        public Defines.EnemyFighterType pEnemyFighterFlight { get => _EnemyFighterFlight; }

        [SerializeField]        private Slider mHealthSlider;
        public Slider pHealthSlider { get => mHealthSlider;}

        private List<Defines.BulletType> mEnemyBulletTypeList;
        private Defines.BulletType aRandBulletType = Defines.BulletType.NONE;

        private GameplayController mGameplayObj;
        private GameManager mGameManager;

        private GameLevelData.DataInfo mLevelInfo;
        private ObjectPooler mObjPool = null;

        private bool isInPlayMode = false;
        private float FlightHealth = 0;
        private bool IsBulletFired = false;

        public override void Awake()
        {
            base.Awake();

            mGameManager = GameManager.Instance;

            int level = StaticGameData.Instance.GetGameplayStats().Level;
            mLevelInfo = StaticGameData.Instance.pLevelData.GetLevelDataInfo(level);

            UpdateHealth();

            _FlightInfo = StaticGameData.Instance.pFighterPlaneData.GetEnemyFlightDataInfo(_EnemyFighterFlight);
            FlightHealth = _FlightInfo.HealthPoints;

            mEnemyBulletTypeList = new List<Defines.BulletType>();
            mEnemyBulletTypeList = _FlightInfo.BulletShootType;

            mHealthSlider.maxValue = _FlightInfo.HealthPoints;
            mHealthSlider.value = mHealthSlider.maxValue;

        }

        private void Start()
        {          
            mGameplayObj = mGameManager.GetGameplayController();
            mObjPool = mGameplayObj.GetObjPooler();

            for (int i = 0; i < mEnemyBulletTypeList.Count; i++)
            {
                mObjPool.CreateBulletObjects(mEnemyBulletTypeList[i]);
            }
        }
        /// <summary>
        /// Health Gets updated by a multiplier according to the level
        /// </summary>
        private void UpdateHealth()
        {
            FlightHealth *= mLevelInfo.HealthMultiplier;
        }
        #region Base Class Functionality
        /// <summary>
        /// Bullet gets fire
        /// </summary>
        public override void FireBullet()
        {
            if (mEnemyBulletTypeList != null && mEnemyBulletTypeList.Count > 0)
            {
                aRandBulletType = mEnemyBulletTypeList[Random.Range(0, mEnemyBulletTypeList.Count)];

                Vector3 pos = mGameplayObj.GetPlayerFighterPlane().transform.position;
                float speed = StaticGameData.Instance.pBulletData.GetBulletDataInfo(aRandBulletType).BulletTimeToReachDestination * mLevelInfo.BulletTimeTravelMultiplier;
                int freq = mLevelInfo.BulletShootFrequency;
                ShootBulletNow(pos, speed,freq);
            }
        }
        /// <summary>
        /// invoke when the health is 0 or less
        /// </summary>
        public override void FighterFlightDestroyed()
        {
            //show custom VFX when called.
            isInPlayMode = false;
            mGameplayObj.CalculatePlayerScore(_EnemyFighterFlight);
            base.FighterFlightDestroyed();
        }

        public override float GetHealthPoints()
        {
            return base.GetHealthPoints();
        }
        /// <summary>
        /// Method gets invoked everytime the bullet hits the Flight
        /// </summary>
        /// <param name="points"></param>
        public override void HitByBullet(float points)
        {
            base.HitByBullet(points);
            if (PlayerPrefs.GetInt(GameKeys.GAME_SOUND_VALUE, 1) == 1)
            {
                SoundManager.Instance.PlaySound(Defines.AudioClips.HIT_BY_BULLET);
            }
            FlightHealth -= points;
            CheckForHealth(FlightHealth);
            mHealthSlider.value -= points;
        }
        /// <summary>
        /// Check the collision object
        /// </summary>
        /// <param name="collision"></param>
        public override void OnCollisionEnter(Collision collision)
        {
            if (!isInPlayMode)
                return;
            if (mGameplayObj.GetCurrGameState() == Defines.GameStates.END)
                return;

            if (collision.gameObject.tag == "PlayerBullet")
            {
                Debug.Log("Enemy: hit by bullet");
                collision.gameObject.SetActive(false);
                this.HitByBullet(collision.gameObject.GetComponent<Bullet>().pDamageUnits);
            }
        }

        #endregion

        private void ShootBulletNow(Vector3 pos, float speed, int count)
        {
            Vector3 ObjPos = this.gameObject.transform.position;
            mObjPool.SpawnFromPool(aRandBulletType, ObjPos, Quaternion.identity, false, pos, speed);
        }

        /// <summary>
        /// Check the fighter plane health
        /// </summary>
        /// <param name="flightHealth"></param>
        void CheckForHealth(float flightHealth)
        {
            if (flightHealth <= 0)
            {
                this.FighterFlightDestroyed();
            }
        }
        private void Update()
        {
            if (mGameManager.IsGamePause())
                return;
            if (mGameplayObj.GetCurrGameState() == Defines.GameStates.END)
                return;

            if (this.transform.position.z <= 200 && !IsBulletFired && this.gameObject.activeSelf)
            {
                IsBulletFired = true;
                FireBullet();

                if (_EnemyFighterFlight == Defines.EnemyFighterType.BOSS)
                {
                    EnemyBossFired();
                }
            }
        }
        /// <summary>
        /// Set The flight if in play mode to the player
        /// </summary>
        public void MoveEnemy()
        {
            isInPlayMode = true;
        }

        public void Reset()
        {
            IsBulletFired = false;
            isInPlayMode = false;
            this.gameObject.SetActive(true);
        }

        void EnemyBossFired()
        {
            StartCoroutine(BossDelayToShoot(3f));
        }
        IEnumerator BossDelayToShoot(float aDelay)
        {
            yield return new WaitForSeconds(aDelay);
            Reset();
        }
    }
}
