﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;

public class EnemyController : MonoBehaviour
{
    [SerializeField]    private EnemyFighterPlane _SmallFighterPlanePrefab;
    [SerializeField]    private EnemyFighterPlane _MediumFighterPlanePrefab;   
    [SerializeField]    private EnemyFighterPlane _BossFighterPlanePrefab;

    [SerializeField]    private GameObject _SmallFighterPlaneParent;
    [SerializeField]    private GameObject _MediumFighterPlaneParent;
    [SerializeField]    private GameObject _BossFighterPlaneParent;

    private List<EnemyFighterPlane> mSmallFighterPlaneList;
    private List<EnemyFighterPlane> mMediumFighterPlaneList;
    private EnemyFighterPlane mBossFighterPlane;

    private List<RectTransform> mSmallEnemiesHealthSliderUI;
    private List<RectTransform> mMediumEnemiesHealthSliderUI;
    private RectTransform mBossHealthSliderUI;

    private Camera _cam = null;
    private GameLevelData.DataInfo mLevelInfo;

    private float SmallPlaneSpeed;
    private float MediumPlaneSpeed;

    private float BossPlaneSpeed;
    private bool isAllEnemySpawnedAndReady = false;

    private float SmallEnemiesPosY;
    private float MediumEnemiesPosY;

    private float SmallEnemyScaleValForUI;
    private float MediumEnemyScaleValForUI;
    private float BossEnemyScaleValForUI;

    private int SmallFlightOccuringCounter = 0;
    private int MediumFlightOccuringCounter = 0;

    private Defines.EnemyFormationType LevelFormation = Defines.EnemyFormationType.NONE;

    private GameManager mGameManager;

    #region Enemy Controller Initialise
    private void Awake()
    {
        mGameManager = GameManager.Instance;
    }
    /// <summary>
    /// Use to Initialise Enemy Control
    /// </summary>
    private void Start()
    {
        isAllEnemySpawnedAndReady = false;

        mSmallFighterPlaneList = new List<EnemyFighterPlane>();
        mMediumFighterPlaneList = new List<EnemyFighterPlane>();

        mSmallEnemiesHealthSliderUI = new List<RectTransform>();
        mMediumEnemiesHealthSliderUI = new List<RectTransform>();

        _cam = Camera.main;
        SetEnemyData();
        SetEnemyUI();

        MoveEnemy(Defines.EnemyFighterType.TYPE_1);
    }
    #endregion

    #region Caching Level Data and Enemy Spawing
    /// <summary>
    /// Caching Level Data to use
    /// </summary>
    void SetEnemyData()
    {
        int level = StaticGameData.Instance.GetGameplayStats().Level;
        mLevelInfo = StaticGameData.Instance.pLevelData.GetLevelDataInfo(level);

        int smallCount = mLevelInfo.SmallEnemyCount;
        int mediumCount = mLevelInfo.MediumEnemyCount;

        SmallFlightOccuringCounter = mLevelInfo.InfoEnemyOccurance.SmallEnemyOccuringCounter;
        MediumFlightOccuringCounter = mLevelInfo.InfoEnemyOccurance.MediumEnemyOccuringCounter;
        LevelFormation = mLevelInfo.InfoEnemyOccurance.CurrFormation;

        FighterPlaneData data =  StaticGameData.Instance.pFighterPlaneData;

        SmallPlaneSpeed = data.GetEnemyFlightDataInfo(Defines.EnemyFighterType.TYPE_1).FlyingSpeed;
        MediumPlaneSpeed = data.GetEnemyFlightDataInfo(Defines.EnemyFighterType.TYPE_2).FlyingSpeed;
        BossPlaneSpeed = data.GetEnemyFlightDataInfo(Defines.EnemyFighterType.BOSS).FlyingSpeed;

        SpawnLevelEnemy(smallCount, mediumCount);
    }
    /// <summary>
    /// Handle Creation of Enemy per Level. 
    /// Should be Called only Once per Load.
    /// </summary>
    /// <param name="smallSize"></param>
    /// <param name="mediumSize"></param>
    public void SpawnLevelEnemy(int smallSize, int mediumSize)
    {
        float PosZ = 450f;
        float ExtraUnits = 10f;
  
        for (int i = 0; i < smallSize; i++)
        {
            GameObject gObj = Instantiate(_SmallFighterPlanePrefab.gameObject, _SmallFighterPlaneParent.transform);
            gObj.transform.position = new Vector3(-100, _SmallFighterPlanePrefab.transform.position.y, PosZ + (i * (gObj.transform.localScale.z + ExtraUnits)));
            mSmallFighterPlaneList.Add(gObj.GetComponent<EnemyFighterPlane>());
        }
        SmallEnemyScaleValForUI = mSmallFighterPlaneList[0].transform.localScale.z;
        SmallEnemiesPosY = smallSize * (SmallEnemyScaleValForUI + (smallSize * ExtraUnits));

        for (int i = 0; i < mediumSize; i++)
        {
            GameObject gObj = Instantiate(_MediumFighterPlanePrefab.gameObject, _MediumFighterPlaneParent.transform);
            gObj.transform.position = new Vector3(100, _MediumFighterPlanePrefab.transform.position.y, PosZ + (i * (gObj.transform.localScale.z + ExtraUnits)));
            mMediumFighterPlaneList.Add(gObj.GetComponent<EnemyFighterPlane>());
        }
        MediumEnemyScaleValForUI = mMediumFighterPlaneList[0].transform.localScale.z;
        MediumEnemiesPosY = mediumSize * (MediumEnemyScaleValForUI + (mediumSize * ExtraUnits));

        GameObject gObjBoss = Instantiate(_BossFighterPlanePrefab.gameObject, _BossFighterPlaneParent.transform);
        gObjBoss.transform.position = new Vector3(0, _BossFighterPlanePrefab.transform.position.y, PosZ);
        mBossFighterPlane = gObjBoss.GetComponent<EnemyFighterPlane>();
        BossEnemyScaleValForUI = mBossFighterPlane.transform.localScale.z;
    }
    #endregion

    #region Enemy Health
    /// <summary>
    /// Setting of Enemy Health UI from Data
    /// </summary>
    void SetEnemyUI()
    {
        if (mSmallFighterPlaneList != null && mSmallFighterPlaneList.Count > 0)
        {
            for (int i = 0; i < mSmallFighterPlaneList.Count; i++)
            {
                mSmallEnemiesHealthSliderUI.Add(mSmallFighterPlaneList[i].pHealthSlider.GetComponent<RectTransform>());
            }
        }
        if (mMediumFighterPlaneList != null && mMediumFighterPlaneList.Count > 0)
        {
            for (int i = 0; i < mMediumFighterPlaneList.Count; i++)
            {
                mMediumEnemiesHealthSliderUI.Add(mMediumFighterPlaneList[i].pHealthSlider.GetComponent<RectTransform>());
            }
        }
        mBossHealthSliderUI = mBossFighterPlane.pHealthSlider.GetComponent<RectTransform>();
        isAllEnemySpawnedAndReady = true;
    }
    /// <summary>
    /// Use to render Enemy health UI on screen.
    /// </summary>
    private void Update()
    {
        if (mGameManager.IsGamePause())
            return;

        if (!isAllEnemySpawnedAndReady)
            return;

        if (mGameManager.GetGameplayController() != null && mGameManager.GetGameplayController().GetCurrGameState() == Defines.GameStates.END)
            return;

        if (mSmallFighterPlaneList != null && mSmallFighterPlaneList.Count > 0)
        {
            for (int i = 0; i < mSmallFighterPlaneList.Count; i++)
            {
                Vector3 pos = mSmallFighterPlaneList[i].transform.position;
                float scaleVal = SmallEnemyScaleValForUI * 2.5f;
                Vector3 screenDist = _cam.WorldToScreenPoint(pos);
                mSmallEnemiesHealthSliderUI[i].position = new Vector2(screenDist.x, screenDist.y - scaleVal);
            }
        }

        if (mMediumFighterPlaneList != null && mMediumFighterPlaneList.Count > 0)
        {
            for (int i = 0; i < mMediumFighterPlaneList.Count; i++)
            {
                Vector3 pos = mMediumFighterPlaneList[i].transform.position;
                float scaleVal = MediumEnemyScaleValForUI * 2.5f;
                Vector3 screenDist = _cam.WorldToScreenPoint(pos);
                mMediumEnemiesHealthSliderUI[i].position = new Vector2(screenDist.x, screenDist.y - scaleVal);
            }
        }

        if (mBossFighterPlane != null)
        {
            Vector3 pos = mBossFighterPlane.transform.position;
            float scaleVal = BossEnemyScaleValForUI * 2.5f;
            Vector3 screenDist = _cam.WorldToScreenPoint(pos);
            mBossHealthSliderUI.position = new Vector2(screenDist.x, screenDist.y - scaleVal);
        }

    }
    #endregion

    #region Enemy Movement
    /// <summary>
    /// Handles the Enemy Movement
    /// </summary>
    /// <param name="aType"></param>
    public void MoveEnemy(Defines.EnemyFighterType aType)
    {
        switch (aType)
        {
            case Defines.EnemyFighterType.NONE:
                break;
            case Defines.EnemyFighterType.TYPE_1:
                for (int i = 0; i < _SmallFighterPlaneParent.transform.childCount; i++)
                {
                    _SmallFighterPlaneParent.transform.GetChild(i).GetComponent<EnemyFighterPlane>().MoveEnemy();
                }
                LeanTween.moveLocalZ(_SmallFighterPlaneParent, -(SmallEnemiesPosY * 2), SmallPlaneSpeed).setOnComplete(()=> {
                    _SmallFighterPlaneParent.transform.position = Vector3.zero;

                    for (int i = 0; i < mSmallFighterPlaneList.Count; i++)
                    {
                        mSmallFighterPlaneList[i].Reset();
                    }
                    UpdateEnemyMovement(aType);

                });                
                break;
            case Defines.EnemyFighterType.TYPE_2:
                for (int i = 0; i < _MediumFighterPlaneParent.transform.childCount; i++)
                {
                    _MediumFighterPlaneParent.transform.GetChild(i).GetComponent<EnemyFighterPlane>().MoveEnemy();
                }
                LeanTween.moveLocalZ(_MediumFighterPlaneParent, -(MediumEnemiesPosY * 2), MediumPlaneSpeed).setOnComplete(() => {
                    _MediumFighterPlaneParent.transform.position = Vector3.zero;

                    for (int i = 0; i < mMediumFighterPlaneList.Count; i++)
                    {
                        mMediumFighterPlaneList[i].Reset();
                    }
                    UpdateEnemyMovement(aType);
                });
                break;
            case Defines.EnemyFighterType.BOSS:

                _BossFighterPlaneParent.transform.GetChild(0).GetComponent<EnemyFighterPlane>().MoveEnemy();                
                LeanTween.moveLocalZ(_BossFighterPlaneParent, -300, BossPlaneSpeed);
                break;
            default:
                break;
        }
  
    }
    #endregion

    #region Enemy Formation
    /// <summary>
    /// Handle the Formation of the Enemies
    /// </summary>
    /// <param name="acurrType"></param>
    void UpdateEnemyMovement(Defines.EnemyFighterType aCurrType)
    {
        switch (aCurrType)
        {
            case Defines.EnemyFighterType.TYPE_1:

                SmallFlightOccuringCounter -= 1;
                switch (LevelFormation)
                {
                    case Defines.EnemyFormationType.NONE:
                        break;
                    case Defines.EnemyFormationType.ALL_IN_A_LOOP:

                        if (SmallFlightOccuringCounter >= 1)// Greater than equal to 1 is because on movement of type 2 enemies have already been moved.So we need 2 to move again.
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_1);
                        }
                        else if (MediumFlightOccuringCounter > 0)
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_2);
                        }
                        else
                        {
                            MoveEnemy(Defines.EnemyFighterType.BOSS);
                        }

                        break;
                    case Defines.EnemyFormationType.ALTERNTIVE_LOOP:
                        if (MediumFlightOccuringCounter > 0)
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_2);
                        }
                        else if (SmallFlightOccuringCounter > 1)
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_1);
                        }
                        else
                        {
                            MoveEnemy(Defines.EnemyFighterType.BOSS);
                        }
                        break;
                    default:
                        break;
                }

                break;
            case Defines.EnemyFighterType.TYPE_2:
                MediumFlightOccuringCounter -= 1;
                switch (LevelFormation)
                {
                    case Defines.EnemyFormationType.NONE:
                        break;
                    case Defines.EnemyFormationType.ALL_IN_A_LOOP:

                        if (MediumFlightOccuringCounter >= 1)// Greater than 1 is because on movement of type 2 enemies have already been moved.So we need 2 to move again.
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_2);
                        }
                        else if (SmallFlightOccuringCounter > 0)
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_1);
                        }
                        else
                        {
                            MoveEnemy(Defines.EnemyFighterType.BOSS);
                        }

                        break;
                    case Defines.EnemyFormationType.ALTERNTIVE_LOOP:
                        if (SmallFlightOccuringCounter > 0)
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_1);
                        }
                        else if (MediumFlightOccuringCounter > 1)
                        {
                            MoveEnemy(Defines.EnemyFighterType.TYPE_2);
                        }
                        else
                        {
                            MoveEnemy(Defines.EnemyFighterType.BOSS);
                        }
                        break;
                    default:
                        break;
                }

                break;
        }
    }
    #endregion    

}
