﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace AirStrikeGlu
{ 
    public class GameplayController : MonoBehaviour
    {
        [SerializeField]    private ObjectPooler _ObjPool;
        [SerializeField]    private GameObject _PlayerFighterPlaneParent;
        [SerializeField]    private GameObject _EnemyFighterPlaneParent;
        [SerializeField]    private EnemyController _EnemyController;
        [SerializeField]    private PlayerController _PlayerController;

        public PlayerController pPlayerController { get => _PlayerController; }
        public EnemyController pEnemyController { get => _EnemyController; }

        private GameManager mGameManager;


        #region Game State Event
        public UnityAction<Defines.GameStates> OnGameStateChangeEvent;
        public void RegisterGameStateChange(UnityAction<Defines.GameStates> aCallBack)
        {
            if (aCallBack != null)
            {
                OnGameStateChangeEvent += aCallBack;
            }
        }
        public void UnRegisterGameStateChange(UnityAction<Defines.GameStates> aCallBack)
        {
            if (aCallBack != null)
            {
                OnGameStateChangeEvent -= aCallBack;
            }
        }
        #endregion

        #region Score Update Event
        public UnityAction<int> OnScoreUpdatedEvent;
        public void RegisterScoreUpdated(UnityAction<int> aCallBack)
        {
            if (aCallBack != null)
            {
                OnScoreUpdatedEvent += aCallBack;
            }
        }
        public void UnRegisterScoreUpdated(UnityAction<int> aCallBack)
        {
            if (aCallBack != null)
            {
                OnScoreUpdatedEvent -= aCallBack;
            }
        }
        #endregion

        #region Game Result Event
        public UnityAction<Defines.GameResult> OnGameResultEvent;
        public void RegisterGameResultState(UnityAction<Defines.GameResult> aCallBack)
        {
            if (aCallBack != null)
            {
                OnGameResultEvent += aCallBack;
            }
        }
        public void UnRegisterGameResultState(UnityAction<Defines.GameResult> aCallBack)
        {
            if (aCallBack != null)
            {
                OnGameResultEvent -= aCallBack;
            }
        }
        #endregion

        private int PlayerScore = 0;

        private GameLevelData.DataInfo mLevelInfo;
        private Defines.GameResult mGameResult = Defines.GameResult.NONE;

        /// <summary>
        /// This method is for setting the game result
        /// </summary>
        /// <param name="aResult"></param>
        public void SetGameResult(Defines.GameResult aResult)
        {
            mGameResult = aResult;
            if (OnGameResultEvent != null)
            {
                OnGameResultEvent.Invoke(aResult);
            }
        }

        public Defines.GameResult GetGameResult()
        {
            return mGameResult;
        }


        private void Awake()
        {
            mGameManager = GameManager.Instance;
            GameManager.Instance.SetGameplayController(this);

            int level = StaticGameData.Instance.GetGameplayStats().Level;
            mLevelInfo = StaticGameData.Instance.pLevelData.GetLevelDataInfo(level);
        }
        #region Player Fighter Detail
        public PlayerFighterPlane GetPlayerFighterPlane()
        {
            return _PlayerController.pPlayerFlight;
        }
        #endregion

        #region Game State Property
        private Defines.GameStates mGameState = Defines.GameStates.NONE;
        public void SetCurrGameState(Defines.GameStates aState)
        {
            mGameState = aState;
            if (OnGameStateChangeEvent != null) {
                OnGameStateChangeEvent.Invoke(mGameState);
            }
            if (mGameState == Defines.GameStates.END)
            {
                for (int i = 0; i < _EnemyFighterPlaneParent.transform.childCount; i++)
                {
                    LeanTween.cancel(_EnemyFighterPlaneParent.transform.GetChild(i).gameObject);
                }
                if (mGameResult == Defines.GameResult.WIN)
                {
                    HideAllEnemies();
                    if (PlayerPrefs.GetInt(GameKeys.GAME_SOUND_VALUE, 1) == 1)
                    {
                        SoundManager.Instance.PlaySound(Defines.AudioClips.GAME_WIN);
                    }
                    StartCoroutine(PlayerMoveOnMatchEnd(2));
                }
                else
                {
                    if (PlayerPrefs.GetInt(GameKeys.GAME_SOUND_VALUE, 1) == 1)
                    {
                        SoundManager.Instance.PlaySound(Defines.AudioClips.GAME_LOSS);
                    }
                    StartCoroutine(DelayLoadHome(2));
                }
            }
        }
        public Defines.GameStates GetCurrGameState()
        {
            return mGameState;
        }
        #endregion

        private void HideAllEnemies()
        {
            _EnemyFighterPlaneParent.SetActive(false);
        }

        public ObjectPooler GetObjPooler()
        {
            return _ObjPool;
        }

        public void CalculatePlayerScore(Defines.EnemyFighterType aType)
        {
            switch (aType)
            {
                case Defines.EnemyFighterType.NONE:
                    break;
                case Defines.EnemyFighterType.TYPE_1:
                    PlayerScore += mLevelInfo.SmallEnemyDestroyPoint;
                    break;
                case Defines.EnemyFighterType.TYPE_2:
                    PlayerScore += mLevelInfo.SmallEnemyDestroyPoint;
                    break;
                case Defines.EnemyFighterType.BOSS:
                    PlayerScore += mLevelInfo.BossEnemyDestroyPoint;
                    SetGameResult(Defines.GameResult.WIN);
                    SetCurrGameState(Defines.GameStates.END);
                    break;
                default:
                    break;
            }
            if (OnScoreUpdatedEvent != null)
            {
                OnScoreUpdatedEvent.Invoke(PlayerScore);
            }
        }

        public int GetPlayerTotalScore()
        {
            return PlayerScore;
        }

        IEnumerator PlayerMoveOnMatchEnd(float aDelay)
        {
            yield return new WaitForSeconds(aDelay);
            LeanTween.moveLocalZ(GetPlayerFighterPlane().gameObject,600,1).setOnComplete(()=> {
                StartCoroutine(DelayLoadHome(2));
            });
        }

        public IEnumerator DelayLoadHome(float aDelay)
        {
            yield return new WaitForSeconds(aDelay);
            mGameManager.SetGameplayController(null);
            SceneLoadingManager.Instance.SwitchScene(GameKeys.SCENE_MAIN_MENU, LoadSceneMode.Single);
        }

    }
}
