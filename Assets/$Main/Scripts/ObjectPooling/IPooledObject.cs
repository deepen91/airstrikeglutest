﻿using UnityEngine;

public interface IPooledObject
{
    void OnPlayerBulletSpawn(Vector3 dir, float travelSpeed);
    void OnEnemyBulletSpawn(Vector3 dir, float travelSpeed);

}
