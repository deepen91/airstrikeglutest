﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public Defines.BulletType type;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<Defines.BulletType, Queue<GameObject>> poolDictionary;

    private void Start()
    {
        poolDictionary = new Dictionary<Defines.BulletType, Queue<GameObject>>();
    }
    /// <summary>
    /// Create and initialise the objects when player/enemy is loaded 
    /// </summary>
    /// <param name="aBulletType"></param>
    internal void CreateBulletObjects(Defines.BulletType aBulletType)
    {
 
        if (poolDictionary.ContainsKey(aBulletType))
            return;

            Pool pool = pools.Find(x => x.type == aBulletType);

            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.transform.SetParent(this.transform);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.type, objectPool);
       
    }

    /// <summary>
    /// Objects get activated from this method and performs the specific tasks.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="pos"></param>
    /// <param name="rot"></param>
    /// <param name="isPlayer"></param>
    /// <param name="dir"></param>
    /// <param name="travelSpeed"></param>
    /// <returns></returns>
    public GameObject SpawnFromPool(Defines.BulletType type, Vector3 pos, Quaternion rot, bool isPlayer, Vector3 dir, float travelSpeed)
    {
        if (!poolDictionary.ContainsKey(type))
        {
            Debug.LogError("Pool with tag " + type + " doesn't exist");
            return null;
        }

        GameObject objToSpawn =  poolDictionary[type].Dequeue();
        objToSpawn.SetActive(true);
        objToSpawn.transform.position = pos;
        objToSpawn.transform.rotation = rot;

        IPooledObject pooledObj = objToSpawn.GetComponent<IPooledObject>();
        if (null != pooledObj)
        {
            if (isPlayer)
            {
                pooledObj.OnPlayerBulletSpawn(dir, travelSpeed);
            }
            else
            {
                pooledObj.OnEnemyBulletSpawn(dir, travelSpeed);
            }
        }

        poolDictionary[type].Enqueue(objToSpawn);
        return objToSpawn;
    }


}
