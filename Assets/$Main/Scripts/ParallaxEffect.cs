﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;

public class ParallaxEffect : MonoBehaviour
{
    public enum EParallaxDirection
    {
        None,
        Up,
        Down,
    }

    [SerializeField]    private GameObject[] mPoolParallax;
    [SerializeField]    private EParallaxDirection currentParallaxDirection = EParallaxDirection.None;
    [SerializeField]    private float mParallaxSpeed;

    private Vector3 firstPos;
    private Vector3 lastPos;

    private GameManager mGameManager;

    private void Awake()
    {
        mGameManager = GameManager.Instance;
        firstPos = new Vector3(mPoolParallax[0].transform.position.x, mPoolParallax[0].transform.position.y, mPoolParallax[0].transform.position.z);//cache the position of the first parallax object.
        lastPos = new Vector3(mPoolParallax[mPoolParallax.Length - 1].transform.position.x, mPoolParallax[mPoolParallax.Length - 1].transform.position.y, mPoolParallax[mPoolParallax.Length - 1].transform.position.z * 2);//cache the position of the last parallax object.
    }

    private void Update()
    {
        if (mGameManager.IsGamePause())
            return;
        if (mGameManager.GetGameplayController() != null && mGameManager.GetGameplayController().GetCurrGameState() == Defines.GameStates.IN_PROGRESS)
        {
            MoveParallax();
            CheckForRearrangement();
        }
    }

    /// <summary>
    /// This Method is responsible for movement of parallax.
    /// </summary>
    void MoveParallax()
    {
        for (int i = 0; i < mPoolParallax.Length; i++)
        {
            mPoolParallax[i].transform.position = Vector3.MoveTowards(mPoolParallax[i].transform.position, lastPos, mParallaxSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// This Method checks for the parallax object and rearrange its pposition.
    /// </summary>
    void CheckForRearrangement()
    {
        for (int i = 0; i < mPoolParallax.Length; i++)
        {
            if (mPoolParallax[i].transform.position.z <= lastPos.z)
            {
                mPoolParallax[i].transform.position = new Vector3(mPoolParallax[i].transform.position.x, mPoolParallax[i].transform.position.y, firstPos.z);
            }
        }
    }

}
