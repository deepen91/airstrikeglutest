﻿public class Defines
{  
    public enum EnemyFighterType
    {
      NONE,
      TYPE_1,
      TYPE_2,
      BOSS
    };

    public enum BulletType
    {
        NONE,
        TYPE_A,
        TYPE_B,
        TYPE_C,
        TYPE_D,
        TYPE_E
    };

    public enum PlayerFighterType
    {
        NONE,
        BASIC,
        MEDIUM,
        ADVANCE
    };

    public enum EnemyFormationType
    {
        NONE,
        ALL_IN_A_LOOP,
        ALTERNTIVE_LOOP
    }

    public enum MainMenuScreen {
        NONE,
        MAIN_MENU_LANDING_SCREEN,
        PLAYER_FIGHTER_SELECTION_SCREEN,
        SETTINGS_SCREEN
    }

    public enum AudioClips
    {
        NONE,
        BG_MUSIC,
        HIT_BY_BULLET,
        GAME_WIN,
        GAME_LOSS
    }
    public enum GameStates
    {
        NONE,
        IN_PROGRESS,
        END
    }

    public enum GameResult
    {
        NONE,
        WIN,
        LOSE
    }

}
