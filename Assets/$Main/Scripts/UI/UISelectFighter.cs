﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelectFighter : MonoBehaviour
{
    private UIHome homeRef;
    private void Awake()
    {
        homeRef = GetComponentInParent<UIHome>();
    }
    public void OnBackBtnClicked()
    {
        homeRef.ChangeUIScreen(Defines.MainMenuScreen.MAIN_MENU_LANDING_SCREEN);
    }
    #region Physical Back Button
#if UNITY_ANDROID
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackBtnClicked();
        }
    }
#endif
    #endregion
}
