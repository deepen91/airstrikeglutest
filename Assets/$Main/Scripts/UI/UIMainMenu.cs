﻿using AirStrikeGlu;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMainMenu : MonoBehaviour
{
    [SerializeField]    private GameObject _PlayBtn;
    [SerializeField]    private GameObject _SelectFighterPlayerBtn;
    [SerializeField]    private Dropdown _LevelDropDown;
    [SerializeField]    private Dropdown _FighterPlaneDropDown;
    [SerializeField]    private Text _HighestScoreTxt;

    private UIHome mHomeRef;

    private void Awake()
    {
        mHomeRef = GetComponentInParent<UIHome>();
    }
    private void Start()
    {
        List<Dropdown.OptionData> dataList = new List<Dropdown.OptionData>();
        List<Defines.PlayerFighterType> tempLst = GetFighterTypeDataList();
        for (int i = 0; i < tempLst.Count; i++)
        {
            Dropdown.OptionData data = new Dropdown.OptionData(tempLst[i].ToString());
            dataList.Add(data);
        }
        _FighterPlaneDropDown.AddOptions(dataList);

        SetCurrentGameLevelUI();
        SetCurrentGameFightUI();
        SethighestScoreUI();
    }
    private void SethighestScoreUI()
    {
        int highScore = ProfileManager.Instance.GetPlayerHighestScore();
        _HighestScoreTxt.text = "HIGHEST SCORE : " + highScore;
    }

    private void SetCurrentGameLevelUI()
    {
        int currLevelIndex = ProfileManager.Instance.GetCurrentGameLevel();
        _LevelDropDown.value = currLevelIndex;
    }
    private void SetCurrentGameFightUI()
    {
        int currFlightIndex = ProfileManager.Instance.GetCurrentGameFlightUsed();
        _FighterPlaneDropDown.value = currFlightIndex;
    }

    private  List<Defines.PlayerFighterType> GetFighterTypeDataList()
    {
        List<Defines.PlayerFighterType> deliveryTypeLst = new List<Defines.PlayerFighterType>() {
            Defines.PlayerFighterType.BASIC,
            Defines.PlayerFighterType.MEDIUM,
            Defines.PlayerFighterType.ADVANCE,    
        };
        return deliveryTypeLst;
    }

    public void OnPlayBtnClicked()
    {
        int newLevelIndex = _LevelDropDown.value;
        Defines.PlayerFighterType newFlightIndex = (Defines.PlayerFighterType)Enum.Parse(typeof(Defines.PlayerFighterType), _FighterPlaneDropDown.captionText.text);

        ProfileManager.Instance.SetCurrentGameFlight(((int)newFlightIndex) - 1);//-1 is done due to starting index of the enum is None.
        ProfileManager.Instance.SetCurrentGameLevel(newLevelIndex);

        GameManager.Instance.pCurrentFighterFlightUsed = newFlightIndex;
        StaticGameData.Instance.SetGameData(newLevelIndex);

        SoundManager.Instance.StopSound();

        SceneLoadingManager.Instance.SwitchScene(GameKeys.SCENE_GAMEPLAY, LoadSceneMode.Single);
    }

    public void OnSettingsBtnClicked()
    {
        mHomeRef.ChangeUIScreen(Defines.MainMenuScreen.SETTINGS_SCREEN);
    }

    public void OnSelectPlayerFighterBtnClicked()
    {
        mHomeRef.ChangeUIScreen(Defines.MainMenuScreen.PLAYER_FIGHTER_SELECTION_SCREEN);
    }

}
