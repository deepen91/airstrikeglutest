﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AirStrikeGlu;
using UnityEngine.SceneManagement;

public class UIGameHud : MonoBehaviour
{
    [SerializeField] private Slider mHealthPointSlider;
    [SerializeField] private Text mHealthPointPercentage;
    [SerializeField] private Color FullHealthColor;
    [SerializeField] private Color HalfHealthColor;
    [SerializeField] private Color LowHealthColor;
    [SerializeField] private Image HealthFillImage;
    [SerializeField] private GameObject _PausePanel;
    [SerializeField] private Text _ScoreTxt;
    [SerializeField] private Button _ShieldBtn;

    private GameplayController mGameplayObj;
    private GameManager mGameManager;
    private PlayerFighterPlane mPlayerPlane;

    private void Start()
    {
        mGameManager = GameManager.Instance;
        mGameplayObj = mGameManager.GetGameplayController();

        if (null != mGameplayObj)
        {
            mPlayerPlane = mGameplayObj.GetPlayerFighterPlane();
            mHealthPointSlider.maxValue = mGameplayObj.GetPlayerFighterPlane().GetHealthPoints();
            mHealthPointSlider.value = mHealthPointSlider.maxValue;

            mGameplayObj.RegisterScoreUpdated(OnScoreUpdate);
            mGameplayObj.RegisterGameStateChange(OnGameStateChange);
            mPlayerPlane.RegisterPlayerHealthUpdated(OnHealthPointUpdated);
            mPlayerPlane.RegisterPlayerShieldState(OnPlayerShieldState);

        }
    }
    private void OnDisable()
    {
        mGameplayObj.UnRegisterScoreUpdated(OnScoreUpdate);
        mGameplayObj.UnRegisterGameStateChange(OnGameStateChange);
        mPlayerPlane.UnRegisterPlayerHealthUpdated(OnHealthPointUpdated);
        mPlayerPlane.RegisterPlayerShieldState(OnPlayerShieldState);

    }

    public void OnHealthPointUpdated(float aValue)
    {
        mHealthPointSlider.value = aValue;
        float percent = ((mHealthPointSlider.value / mHealthPointSlider.maxValue) * 100);
        mHealthPointPercentage.text = Mathf.RoundToInt(percent) + " %";

        if (percent > 60)
        {
            HealthFillImage.color = FullHealthColor;
        }
        else if (percent > 30)
        {
            HealthFillImage.color = HalfHealthColor;
        }
        else
        {
            HealthFillImage.color = LowHealthColor;
        }
    }

    int pauseCount = 0;
    public void OnPauseGame()
    {
        _PausePanel.SetActive(true);
        mGameManager.OnPause(true);
    }

    public void OnQuitGame()
    {
        mGameManager.OnPause(false);
        StartCoroutine(mGameplayObj.DelayLoadHome(0));
    }
    public void OnResumeGame()
    {
        _PausePanel.SetActive(false);
        mGameManager.OnPause(false);
    }

    private void OnScoreUpdate(int aScore)
    {
        _ScoreTxt.text = " SCORE : " + aScore;
    }

    private void OnGameStateChange(Defines.GameStates aStata)
    {
        if (aStata == Defines.GameStates.IN_PROGRESS)
        {
            _ShieldBtn.interactable = true;
        }
    }

    public void OnShieldBtnClicked()
    {
        if (mPlayerPlane != null)
        {
            mPlayerPlane.CreateShield();
        }
    }

    private void OnPlayerShieldState(bool aState)
    {
        _ShieldBtn.interactable = !aState;
    }

}
