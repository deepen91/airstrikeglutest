﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;
using UnityEngine.SceneManagement;

public class UILoadingScene : MonoBehaviour
{
    public GameObject _LoadingPanel;
    public GameObject _LoadingBar;

    private float tempDelay = 1;

    void Start()
    {
        StartCoroutine(Delay(tempDelay));
    }
    IEnumerator Delay(float delayTime)
    {
        ShowLoadingAnimation();
        yield return new WaitForSeconds(delayTime);
        HideLoadingAnimation();
        SceneLoadingManager.Instance.SwitchScene(GameKeys.SCENE_MAIN_MENU, LoadSceneMode.Single);
    }

    float speed = 0.5f;
    float angle = 180;
    void LoadingAnimation()
    {
        LeanTween.rotateZ(_LoadingBar, angle, speed).setOnComplete(() => {
            LeanTween.rotateZ(_LoadingBar, angle * 2, speed).setOnComplete(() => {
                if (_LoadingPanel.activeSelf)
                    LoadingAnimation();
            });
        });
    }
    void ShowLoadingAnimation()
    {
        _LoadingPanel.gameObject.SetActive(true);
        LoadingAnimation();
    }

    void HideLoadingAnimation()
    {
        LeanTween.cancelAll();
        _LoadingPanel.gameObject.SetActive(false);
    }

}
