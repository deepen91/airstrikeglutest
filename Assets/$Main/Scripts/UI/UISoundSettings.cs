﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;
using UnityEngine.UI;
public class UISoundSettings : MonoBehaviour
{
    [SerializeField]    private Toggle _SoundBtn;
    [SerializeField]    private Slider _SoundVol;

    private void OnEnable()
    {
        CheckIfSoundIsActive();
        CheckSoundVolume();
    }

    private void CheckIfSoundIsActive()
    {
        int val = PlayerPrefs.GetInt(GameKeys.GAME_SOUND_VALUE, 1);
        if (val == 0)
        {
            _SoundBtn.isOn = false;
        }
        else
        {
            _SoundBtn.isOn = true;
        }
    }
    private void CheckSoundVolume()
    {
        float val = PlayerPrefs.GetFloat(GameKeys.GAME_SOUND_VOLUME, 1);
        _SoundVol.value = val;
        OnSoundVolumeUpdated();
    }

    public void OnSoundSettingChange()
    {
        if (_SoundBtn.isOn)
        {
            PlayerPrefs.SetInt(GameKeys.GAME_SOUND_VALUE,1);
            SoundManager.Instance.PlaySound(Defines.AudioClips.BG_MUSIC);
        }
        else
        {
            PlayerPrefs.SetInt(GameKeys.GAME_SOUND_VALUE, 0);
            SoundManager.Instance.StopSound();
        }
    }

    public void OnSoundVolumeUpdated()
    {
        SoundManager.Instance.UpdateSoundVolume(_SoundVol.value);
    }
}
