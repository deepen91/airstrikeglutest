﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirStrikeGlu;
using UnityEngine.UI;

public class UIHome : MonoBehaviour
{
    [SerializeField]    private GameObject[] _UI_Screens;

    private Defines.MainMenuScreen currMainMenuScreen = Defines.MainMenuScreen.NONE;

    private void Awake()
    {
        currMainMenuScreen = Defines.MainMenuScreen.MAIN_MENU_LANDING_SCREEN;
        ChangeUIScreen(currMainMenuScreen);
    }
    private void Start()
    {
        if (PlayerPrefs.GetInt(GameKeys.GAME_SOUND_VALUE, 1) == 1)
        {
            SoundManager.Instance.PlaySound(Defines.AudioClips.BG_MUSIC);
        }
    }

    #region UI Screen Navigation
    public void ChangeUIScreen(Defines.MainMenuScreen aScreen)
    {
        if (aScreen == Defines.MainMenuScreen.NONE)
            return;

        currMainMenuScreen = aScreen;
        for (int i = 0; i < _UI_Screens.Length; i++)
        {
            _UI_Screens[i].SetActive(false);
        }
        int index = ((int)aScreen) - 1; //-1 is because the 0th index of enum is NONE.
        _UI_Screens[index].SetActive(true);
    }
    #endregion

}
