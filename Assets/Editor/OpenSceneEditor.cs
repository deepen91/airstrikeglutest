﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class OpenSceneEditor : MonoBehaviour
{

	[MenuItem ("OpenScene/LoadingScene")]
	public static void OpenScene_Launcher ()
	{
		if ( EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo () )
		{
			EditorSceneManager.SaveOpenScenes ();
			EditorSceneManager.OpenScene ("Assets/$Main/$Game/Scenes/LoadingScene.unity");
		}
	}


    [MenuItem ("OpenScene/Gameplay")]
    public static void OpenScene_Bootstrapper ()
    {
        if ( EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo () )
        {
            EditorSceneManager.SaveOpenScenes ();
			EditorSceneManager.OpenScene ("Assets/$Main/$Game/Scenes/Gameplay.unity");
        }
    }

    [MenuItem ("OpenScene/MainMenu")]
	public static void OpenScene_Startup ()
    {
        if ( EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo () )
        {
            EditorSceneManager.SaveOpenScenes ();
			EditorSceneManager.OpenScene ("Assets/$Main/$Game/Scenes/MainMenu.unity");
        }
    }
}
